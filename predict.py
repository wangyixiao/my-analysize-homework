#coding :utf-8
#author :mixian
#email  :951930122@qq.com

'''此文件用于线性回归分析'''
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn import preprocessing
from sklearn.preprocessing import MinMaxScaler
from sklearn.linear_model import LinearRegression


scaler = MinMaxScaler(feature_range=(0,1))#标准化数据

def predict(df):
    df.sort_values(by=['date'], inplace=True, ascending=True)


    numb = 15  # 预测5天后的情况
    df['label1'] = df['close'].shift(-numb)  # 预测值

    Data = df.drop(['label1', 'price_change', 'p_change'], axis=1)
    X1 = Data.values
    X1 = preprocessing.scale(X1)
    X1 = X1[:-numb]#取确定长度数据集

    df.dropna(inplace=True)
    Target = df.label1
    y = Target.values
    # 分割数据集为训练集和预测集
    X_train, y_train = X1[0:550, :], y[0:550]
    X_test, y_test = X1[550:, -51:], y[550:606]
    #线性分析
    Lr = LinearRegression()
    Lr.fit(X_train, y_train)

    X_Predict = X1[-numb:]
    Forecast = Lr.predict(X_Predict)
    # print(Forecast)#检查
    # print(y[-num:])

    tra = pd.date_range('2021-07-01', periods=numb, freq='d')
    # print(tra)

    # 产生预测的数据集
    Predict_df = pd.DataFrame(Forecast, index=tra)
    Predict_df.columns = ['forecast']

    # 将预测值添加到csv文件
    df = pd.read_csv('600848.csv')
    df['date'] = pd.to_datetime(df['date'])
    df = df.set_index('date')

    # 按照时间升序排列
    df.sort_values(by=['date'], inplace=True, ascending=True)
    df_concat = pd.concat([df, Predict_df], axis=1)

    df_concat = df_concat[df_concat.index.isin(Predict_df.index)]
    df_concat.tail(numb)
    #画图
    df_concat['close'].plot(color='green', linewidth=1)
    df_concat['forecast'].plot(color='orange', linewidth=3)
    plt.xlabel('Time')
    plt.ylabel('Price')
    plt.show()

