#coding :utf-8
#author :mixian
#email  :951930122@qq.com

import pandas as pd
import numpy as np
from matplotlib.pyplot import MultipleLocator
import matplotlib.pyplot as plt
import seaborn as sns


def Analyse1(data):#由热力图分析结果制作分析表
    sns.set_style('white',{'font.sans-serif':['simhei','Arial']})#'white, dark, whitegrid, darkgrid, ticks'为可选风格，后一句为设置中文
    sns.set_context('paper')

    plt.figure(figsize=(12,4))
    for i in range(1,4):
        if i == 1: j = 5
        elif i == 2 : j = 10
        else  : j = 20
        plt.subplot(1,3,i)#多图绘制

        sns.regplot(x=data['turnover'], y=data['v_ma{}'.format(j)],x_bins=80)#将数据分为80个块进行拟合
        # plt.xlim(0.980, 1.003)  # 线宽度
        plt.xlabel('转手率')
        plt.ylabel('{}日均量'.format(j))
        plt.title('上海临港转手率-{}日均量分析图'.format(j))
    plt.show()



    '''总体线性相关性较高，在转手率大于1时线性相关程度较高'''

def Analyse2(data):#由热力图分析结果制作分析表
    sns.set_style('white',{'font.sans-serif':['simhei','Arial']})#'white, dark, whitegrid, darkgrid, ticks'为可选风格，后一句为设置中文
    sns.set_context('paper')#文字样式

    plt.figure(figsize=(6,4))

    sns.regplot(x=data['volume'], y=data['high'])#绘制
    plt.xlabel('成交量')
    plt.ylabel('股价最高价')
    plt.title('上海临港成交量-股价分析图')

    plt.show()

    '''数据较为离散，关系不大'''
