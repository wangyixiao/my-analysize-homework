#coding :utf-8
#author :mixian
#email  :951930122@qq.com

from For_data import *
from Basic_chart import *
from Financial_chart import *
from Analyse import  *
from predict import *

'''数据获取'''
# creatdata()#通过api接口获取和生成数据
data = load_data1()#加载数据
data2 = load_data2()
# print(data)
data = DealWithData(data)#数据处理/清洗/测试

'''数据间关系可视化'''



#热力图，分析比较股票数据间影响强度
Variable(data)
#箱型图，分析股票数据各元素特征
BoxPlot(data)

'''数据间关系分析（散点图线性拟合）'''

#转手率/均量分析
Analyse1(data)
#股价/最高价分析
Analyse2(data)


'''股价数据总览与分析'''
#分析股票数据本身特征（风险、收益，走向）
K_Line(data)
#开/收盘以及涨跌幅度列表
Quote_change(data)

# '''股价预测'''
# predict(data)





