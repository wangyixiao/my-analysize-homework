#coding :utf-8
#author :mixian
#email  :951930122@qq.com

import pandas as pd
import numpy as np
import seaborn as sns
from matplotlib.pyplot import MultipleLocator
import matplotlib.pyplot as plt
import mplfinance as mpf#matlabplot金融绘图包


def K_Line(data):#股票常用的k线图绘制，这里用到mplfinance包（原本在matplotlib.finance）
    plt.figure(figsize=(14,9))#定义画布大小
    plt.tight_layout()
    #做一个自己喜欢的界面样式
    '''PS：style基本样式喜欢的三个：mike,nightclouds,yahoo'''
    my_color = mpf.make_marketcolors(up='red',#上涨k线箱体颜色
                                     down='green',
                                     wick='white'#均线线颜色
                                     )
    my_style = mpf.make_mpf_style(base_mpf_style='mike',marketcolors=my_color,gridaxis='both',gridstyle='-.',y_on_right=True)
    # 控制j、k画多个图
    for i in range(1,4):
        if i==1 :
            j = 1
            k = 2
        elif i==2  :
            j = 3
            k = 5
        else :
            j = 12
            k = 20
        ''''''
        #MACD(异同移动平均线)图数据准备
        exp12 = data['close'].ewm(span=12,adjust=False).mean()#快的指数移动平均线
        exp26 = data['close'].ewm(span=26,adjust=False).mean()#慢的指数移动平均线
        macd = exp12 - exp26
        # signal = macd.ewm(span=9,adjust=False).mean()#信号
        # histogram = macd - signal

        #增添图表部分
        add_plot = [
            mpf.make_addplot(data['p_change'][-31*j:],color = 'yellow',alpha = 0.4,ylabel='涨跌(%)',linestyle='-.',panel='lower'),#panel控制图线画的位置为副图表
            mpf.make_addplot(macd[-31*j:],panel=2,color='fuchsia',secondary_y = True,ylabel='MACD线图')#MACD图绘制
        ]

        # mav为添加均线图,ps:"volume=True"为添加'成交量'附表
        mpf.plot(data[-31*j:],type='candle',mav = (2,k), volume=True,addplot=add_plot,
                 style=my_style,title='近{}月上海临港股价K线/成交量/2、{}日均线图表'.format(j,k),
                 ylabel_lower='成交量（股/天）',ylabel='价格(元/股)')

    plt.show()

    '''MACD指数为正的时候购入数显著增加，MACD指数'''


def Quote_change(data):  # 开盘/收盘以及涨跌幅图表
    plt.style.use("ggplot")
    plt.figure(figsize=(12, 8))

    for i in range(1, 5):
        # 控制画图数据
        if i == 1:
            j = 1
        elif i == 2 or i == 3:
             j = 3 * (i - 1)
        else:
            j = 12

        plt.subplot(2, 2, i)
            # 画图
        plt.plot(data['close'][-31 * j:], color='blue', label=u'收盘价(元/每股)', )
        plt.plot(data['open'][-31 * j:], color='red', label=u'开盘价(元/每股)')
        plt.plot(data['p_change'][-31 * j:], color='yellow', label=u'涨跌幅(%/天)')

        x_major_locator = MultipleLocator(7 * j)  # 设置x轴显示间隔参数，以i周为单位

        ax = plt.gca()  # ax为坐标轴示例
        ax.xaxis.set_major_locator(x_major_locator)  # 设置间隔

            # 设置x/y轴标签
        if i == 1:
            plt.xlabel("过去1个月", fontsize=12)
        elif i == 2 or i == 3:
            plt.xlabel('过去{}个月'.format(j), fontsize=12)
        else:
            plt.xlabel('过去1年', fontsize=12)
        plt.ylabel(u'数据', fontsize=12)
        plt.title(u'上海临港控股股份', fontsize=12)

    plt.legend()  # 显示标签
    plt.grid(True)  # 设置网格

    plt.show()
    print("图表八打印完成\n")