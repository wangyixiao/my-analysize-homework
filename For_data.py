#coding :utf-8
#author :mixian
#email  :951930122@qq.com

import tushare as ts
import pandas as pd
import numpy as np
'''本文件用于数据相关操作'''

def creatdata():
    end = '2022-07-26'#设置中止日期
    info = ts.get_hist_data('600848',end=end)#抓取股票数据包
    info.to_csv('600848.csv')#存储到csv文件
    info.to_excel('600848.xlsx')#存储到excel文件

    info1 = ts.get_hist_data('000002',end=end)#上证A指
    info1.to_csv('000002.csv')  # 存储到csv文件

def load_data1():#加载数据
    data = pd.read_csv('600848.csv', sep=',')#加载数据
    # print(data.columns)#检查索引名称
    # print(data.head(5))
    return data

def load_data2():#加载数据
    data = pd.read_csv('000002.csv', sep=',')#加载数据
    # print(data.columns)#检查索引名称
    # print(data.head(5))
    return data

def DealWithData(data):#处理数据

    print('是否存在重复:',any(data.duplicated()))#重复值检查
    data.dropna(how='any',axis=1)#缺失值处理，模式：删除任意缺失值所在行

    # data.columns = columns#修改行索引为中文
    data.set_index(pd.to_datetime(data['date']),inplace=True)#修改列索引为时间(7.27修改为更好方式，原代码data.index = data['date'])
    data = data.iloc[::-1]  # 倒置数据，让时间变为正序
    # print(data.index)
    # print(data.head(5))#查看数据
    # print(data.loc['2022-07-20'])#测试提取单独数据
    return data