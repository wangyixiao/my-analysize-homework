#coding :utf-8
#author :mixian
#email  :951930122@qq.com
'''指用数据集基本数据得出的图标'''
import pandas as pd
from matplotlib.pyplot import MultipleLocator
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns



def BoxPlot(data):#箱线图绘制
    plt.style.use("ggplot")#风格设定
    columns = data.columns.tolist()#将数据集矩阵转化为列表
    print(columns)#查看列表

    fig = plt.figure(figsize=(14,8))#设定画布大小

    for i in range(1,len(columns)):
        #填图
        plt.subplot(7,2,i)
        #箱线图绘制
        sns.boxplot(data[columns[i]],orient="v",width=0.5,color='b')

        plt.ylabel(columns[i],fontsize=12)#设定y轴线
    plt.tight_layout()
    print("图表2完成")
    plt.show()

    '''结论2：股票价格离散程度低，在12-26之间'''
    '''股价变动幅度较大，均量'''




def Variable(data):#热力图，用于寻找数据之间关系
    sns.set_style('dark')
    plt.figure(figsize=(12,8))

    # columns = data.columns.tolist()
    #系数关联性比较
    mcorr = data.corr()#取得整个数据集关联性数据
    mask = np.zeros_like(mcorr,dtype=np.bool8)
    #返回上三角索引
    mask[np.triu_indices_from(mask)] = True
    cmap = sns.diverging_palette(220,10,as_cmap=True)
    G = sns.heatmap(mcorr,mask=mask,cmap=cmap,square=True,annot=True,fmt='0.2f')
    plt.show()
    print("图表1打印完成")

    '''由数据图可见，成交量（volume）与换手率（turnover）都同均量线有着较高联系'''











